import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import ProductTable from './components/ProductTable'
import KnowledgeRepo from './components/KnowledgeRepo'
import * as serviceWorker from './components/serviceWorker';
import './semantic/dist/semantic.min.css';
import './components/KnowledgeRepo.css';

ReactDOM.render(<KnowledgeRepo />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
