import React from 'react';
import hljs from 'highlight.js';
import 'highlight.js/styles/tomorrow.css';
import ReactQuill from 'react-quill';
import './quill.snow.css';

hljs.configure({
  languages: ['javascript']
})

class QuillEditBox extends React.Component {

	defaultToolBar = [['bold', 'italic', 'underline','strike', 'code'],
                    [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
                    ['blockquote','code-block'],
                    ['link', 'image'],
                    ['clean']];

  modules = {
    toolbar: false,
    syntax: { highlight: text => hljs.highlightAuto(text).value },
    clipboard: { matchVisual: false },
  }  

  formats = [
    'bold', 'italic', 'underline', 'strike', 'code', 'blockquote','code-block',
    'list', 'bullet', 'indent',
    'link', 'image'
  ]

  render(){

    if (this.props.readOnly === true){
    	this.modules.toolbar = false
    } else {
    	this.modules.toolbar = this.defaultToolBar
    } 

    var newModules = {...this.modules}; // this is needed in order to force rerender of ReactQuill toolbar when switching between edit mode

  	return <ReactQuill readOnly={this.props.readOnly}
                         modules={newModules}
                         formats={this.formats}
                         value={this.props.value}
                         style={this.props.bordered ? {border : '1px solid lightgray'} : null}
                         onChange={this.props.onChange} />
  }
}

export default QuillEditBox;
