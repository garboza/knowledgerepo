import React from 'react';
import hljs from 'highlight.js';
import 'highlight.js/styles/tomorrow.css';
import ReactQuill from 'react-quill';
import './quill.snow.css';

hljs.configure({
  languages: ['javascript']
})

class QuillCodeBox extends React.Component {

    modules = {
      toolbar: false,
      syntax: { highlight: text => hljs.highlightAuto(text).value },
      clipboard: { matchVisual: false },
    }  

    formats = ['code-block']

    componentDidMount(){
      this.reactQuillRef.getEditor().formatLine(0, this.reactQuillRef.getEditor().getLength(), { 'code-block': true });
    }

    render(){
    	return <ReactQuill ref={(el) => {this.reactQuillRef = el}}
                         readOnly={this.props.readOnly}
                         modules={this.modules}
                         formats={this.formats}
                         value={this.props.value}
                         style={{border : '1px solid lightgray', background: '#f0f0f0'}}
                         onChange={this.props.onChange} />
    }
}

export default QuillCodeBox;
