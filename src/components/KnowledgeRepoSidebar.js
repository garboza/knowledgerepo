import React from 'react';
import { Icon, Menu } from 'semantic-ui-react'

class KnowledgeRepoSidebar extends React.Component {

  handleClick = (selection) => {
    this.props.onChangeMainContent(selection);
  }

  render(){
  return (
      <Menu icon='labeled' vertical secondary inverted>

          <Menu.Item as='a' active={this.props.active === 'quiz'} onClick={() => this.handleClick('quiz')}>
              <Icon name='clock outline' />
              Quiz
          </Menu.Item>
          <Menu.Item as='a' active={this.props.active === 'browser'} onClick={() => this.handleClick('browser')}>
  				    <Icon name='book' />
  				    Knowledge
  				</Menu.Item>

			 </Menu>
		)
	}
}

export default KnowledgeRepoSidebar;