import React from 'react';
import KnowledgeRepoHeader from './KnowledgeRepoHeader';
import KnowledgeRepoSidebar from './KnowledgeRepoSidebar';
import QuizDashboard from './quiz/QuizDashboard';
import KnowledgeSubjectBrowser from './browser/KnowledgeSubjectBrowser';
import './KnowledgeRepo.css';

class KnowledgeRepo extends React.Component {

    state = {mainContent: 'browser'}

    handleChangeMainContent = (newMainContent) => {
        this.setState({mainContent: newMainContent});
    }

    render() {

        return (
            <div className="krcontainer">
                <header>
                    <KnowledgeRepoHeader/>
                </header>
                <nav>
                    <KnowledgeRepoSidebar active={this.state.mainContent}
                                          onChangeMainContent={this.handleChangeMainContent}/>
                </nav>
                <main>
                    {this.state.mainContent === 'quiz' ? <QuizDashboard/> : <KnowledgeSubjectBrowser/>}
                </main>
            </div>
        )
    }
}

export default KnowledgeRepo;
