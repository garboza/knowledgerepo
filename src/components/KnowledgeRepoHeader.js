import React from 'react';
import { Label, Menu } from 'semantic-ui-react';

class KnowledgeRepoHeader extends React.Component {

	render(){
		return (
			<Menu inverted secondary size='small'>
				<Menu.Item position='left'>
					<div className="logo">
						Knowledge Repo
					</div>
				</Menu.Item>
				<Menu.Item position='right'>
					<Label circular color='red' key='red'>
        				J
      				</Label>
				</Menu.Item>
			</Menu>
		)
	}

}

export default KnowledgeRepoHeader;
