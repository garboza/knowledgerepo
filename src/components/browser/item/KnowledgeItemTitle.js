import React from 'react';
import { ReactComponent as KnowledgeItemTitleIcon } from './KnowledgeItemTitleIcon.svg';
import styles from './KnowledgeItem.css'

class KnowledgeItemTitle extends React.Component {

	render(){
		let editable = this.props.mode === 'edit' ? styles.ketitle_textarea_edit : '';

		return (
			<div className={styles.ketitle}>
				<KnowledgeItemTitleIcon className={styles.keicon} width={this.props.size} />
				<textarea className={styles.ketitle_textarea + ' ' + editable} 
						  rows="1" 
						  cols="40" 
						  onChange={this.props.onTitleChange}
						  value={this.props.children}
						  readOnly={this.props.mode === 'edit' ? false : true} />						 
			</div>
		)
	}
}

export default KnowledgeItemTitle;