import React from 'react';
import { ReactComponent as KnowledgeItemDeleteIcon } from './KnowledgeItemDeleteIcon.svg';
import { ReactComponent as KnowledgeItemEditIcon } from './KnowledgeItemEditIcon.svg';
import { ReactComponent as KnowledgeItemAcceptIcon } from './KnowledgeItemAcceptIcon.svg';
import { ReactComponent as KnowledgeItemCancelIcon } from './KnowledgeItemCancelIcon.svg';
import { Button, Modal } from 'semantic-ui-react'
import styles from './KnowledgeItem.css'

class KnowledgeItemActions extends React.Component {

	state = { open: false }

	show = size => () => this.setState({ size, open: true });	
  	close = () => this.setState({ open: false });

  	clickCancel = (index) => {
  		if (this.props.newItem) {
  			this.props.onClickDelete(index);
  		} else {
  			this.props.onClickCancel();
  		}
  	}

	render(){

		const Handle = this.props.handle;
		const { open, size } = this.state;

		if (this.props.mode === 'edit') {
			return (
				<div>
					<KnowledgeItemAcceptIcon className={styles.actionicon} onClick={this.props.onClickAccept} size={this.props.size} />
					<KnowledgeItemCancelIcon className={styles.actionicon} onClick={(e, index) => this.clickCancel(index)} size={this.props.size} />
				</div>
			)
		} else {
			return (
				<div>
					<KnowledgeItemDeleteIcon className={styles.actionicon} size={this.props.size} onClick={this.show('tiny')}/>
					<KnowledgeItemEditIcon className={styles.actionicon} onClick={this.props.onClickEdit} size={this.props.size} /> 
					<Handle />
					
					<Modal dimmer='inverted' closeOnDimmerClick={false} size={size} open={open} onClose={this.close}>
          				<Modal.Header>Delete Knowledge Item</Modal.Header>
          				<Modal.Content>
            				<p>Are you sure you want to permanently delete this knowledge item?</p>
          				</Modal.Content>
          				<Modal.Actions>
            				<Button negative onClick={this.close}>No</Button>
            				<Button positive icon='trash alternate outline' labelPosition='right' content='Delete' onClick={(e, index) => this.props.onClickDelete(index)}/>
          				</Modal.Actions>
        			</Modal>
				</div>
			)
		}
	}
}

export default KnowledgeItemActions;


// <KnowledgeItemDeleteIcon className={styles.actionicon} size={this.props.size} onClick={(e, index) => this.props.onClickDelete(index)}/>