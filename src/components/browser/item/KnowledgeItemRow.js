import React from 'react';
import KnowledgeItemTitle from './KnowledgeItemTitle';
import KnowledgeItemScore from './KnowledgeItemScore';
import KnowledgeItemActions from './KnowledgeItemActions';
import KnowledgeItemEditPanel from '../editpanel/KnowledgeItemEditPanel';
import styles from './KnowledgeItem.css';

// TODO : convert this class to "abbreviated" mode (no constructor, arrow functions, etc)

class KnowledgeItemRow extends React.Component {
	constructor(props){
		super(props);
		this.state = { mouseOver       : false,
					   mode            : this.props.newItem === true ? 'edit' : 'collapse',
					   newItem         : this.props.newItem || false,
					   title           : this.props.title   || '',
					   details         : this.props.details || '',
					   tests           : this.props.tests   || [],
					   score           : this.props.score   || 0,
					   currentTab      : "Description",
					   currentTabIndex : 0 };
		this.onMouseOverKnowledgeItem = this.onMouseOverKnowledgeItem.bind(this);
		this.onMouseOutKnowledgeItem = this.onMouseOutKnowledgeItem.bind(this);
		this.onToggleEdit = this.onToggleEdit.bind(this);
		this.onToggleExpand = this.onToggleExpand.bind(this);
		this.onToggleAccept = this.onToggleAccept.bind(this);
		this.onToggleCancel = this.onToggleCancel.bind(this);
		this.onNewDetails = this.onNewDetails.bind(this);
		this.onNewTitle = this.onNewTitle.bind(this);
		this.onNewTest = this.onNewTest.bind(this);
		this.onNewTestDetails = this.onNewTestDetails.bind(this);
		this.onSelectNewTab = this.onSelectNewTab.bind(this);
		this.getTabIndex = this.getTabIndex.bind(this);
	}

	onMouseOverKnowledgeItem(){
		this.setState( {mouseOver : true} );
	}

	onMouseOutKnowledgeItem(){
		this.setState( {mouseOver : false} );
	}

	onToggleEdit(e){
		e.stopPropagation();
		this.setState( { mode : 'edit' } );
	}

	onToggleExpand(){
		if (this.state.mode === 'collapse') {
			this.setState( { mode : 'expand' });
		} else if (this.state.mode === 'expand') {
			this.setState( { mode : 'collapse' });
		}
	}

	onToggleAccept(){

		// TODO :
		//  x 1) onUpdate is using title, instead it should use id (fixed)
		//  x 2) this will eventually make an API call
		//    3) there should be a check to see if the details actually changed before making an API call

		this.setState( { mode : 'expand', newItem : false } );   // TODO : the mode change should only happen after we have successfully performed the update
		this.props.onUpdate(this.props.id, this.state.title, this.state.details, this.state.tests);
	}

	onToggleCancel(){
		this.setState( { mode            : 'expand',
						 title           : this.props.title,
						 details         : this.props.details,
						 tests           : this.props.tests,
						 currentTab      : "Description",
						 currentTabIndex : 0 } );
	}

	onNewDetails(newDetails){
		this.setState( { details : newDetails } );
	}

	onNewTest(newTest){
		this.setState( { tests           : [...this.state.tests, newTest] ,
		                 currentTabIndex : this.state.tests.length + 1,
		                 currentTab      : "Test " + (this.state.tests.length + 1)} );
	}

	onNewTestDetails(index, newTestDetails){
		let newTests = [...this.state.tests];
		newTests[index] = { ...newTests[index],...newTestDetails };
		this.setState( { tests : newTests });
	}

	onNewTitle(e){
		this.setState( { title : e.target.value });
	}

    onSelectNewTab(e, { name } ){
        this.setState( { currentTab : name,
                         currentTabIndex : this.getTabIndex(name) })
    }

    /* Kinda hacky, but for now it will do
       Basically I am getting the tab index by looking at the tab name
       "Description" is always tab 0
       "Test 1"      is index 1
       "Test 2"      is index 2, etc. */
    getTabIndex(tabName){
        if (tabName === 'Description'){
            return 0;
        } else {
            return tabName.substr(-1);
        }
    }

	render(){
		let iconSize = 20;
		let selectedStyle = (this.state.mode !== 'collapse') ? styles.kerow_selected : '';

		return (
			<div>
				<div onMouseOver={this.onMouseOverKnowledgeItem}
				     onMouseOut={this.onMouseOutKnowledgeItem}
				     onClick={this.onToggleExpand}
				     className={styles.kerow + ' ' + selectedStyle}>
					<KnowledgeItemTitle mode={this.state.mode}
					                    onTitleChange={this.onNewTitle}
					                    size={iconSize}>
						{this.state.title}
					</KnowledgeItemTitle>
					{ this.state.mode !== 'edit' && <KnowledgeItemScore score={this.state.score} />}
					<div className={ (this.state.mouseOver || this.state.mode !== 'collapse') ? styles.keactions_visible : styles.keactions }>
						<KnowledgeItemActions size={iconSize}
						                      mode={this.state.mode}
						                      handle={this.props.handle}
						                      newItem={this.state.newItem}
						                      onClickEdit={this.onToggleEdit}
						                      onClickAccept={this.onToggleAccept}
						                      onClickDelete={this.props.onDelete}
						                      onClickCancel={this.onToggleCancel}/>
					</div>
				</div>
				{ this.state.mode !== 'collapse' && <KnowledgeItemEditPanel mode={this.state.mode}
																			details={this.state.details}
																			tests={this.state.tests}
																			currentTab={this.state.currentTab}
																			currentTabIndex={this.state.currentTabIndex}
																		    onNewDetails={this.onNewDetails}
																		    onNewTest={this.onNewTest}
																		    onNewTestDetails={this.onNewTestDetails}
																		    onSelectNewTab={this.onSelectNewTab} />
				}
			</div>
		)
	}
}

export default KnowledgeItemRow;
