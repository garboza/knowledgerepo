import React from 'react';
import { Progress } from 'semantic-ui-react';
import styles from './KnowledgeItem.css'

class KnowledgeItemScore extends React.Component {

	render(){
		return (
			<div className={styles.kescore}>
				{this.props.score}%
				<Progress percent={this.props.score} color='blue' size='tiny'/>
			</div>
		)
		
	}
}

export default KnowledgeItemScore;