import React from 'react';
import {Grid, Menu, Button, Icon} from 'semantic-ui-react';
import KnowledgeSubjectTree from './KnowledgeSubjectTree';
import KnowledgeItemList from './KnowledgeItemList';
import styles from './KnowledgeSubjectBrowser.css';
import Client from '../Client';
import arrayMove from 'array-move';
import mongoose from 'mongoose';

class KnowledgeSubjectBrowser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedSubject: "",
            selectedTopic: "",
            selectedItems: [],
            subjects: []
        };
    }

    componentDidMount() {
        Client.getSubjects()
            .then(response => {
                const subjects = response.data;
                const firstSubject = subjects[0]['subject'];
                const firstTopic = subjects[0]['topics'][0].name; // TODO : this needs some major error checking
                return {firstSubject, firstTopic, subjects}
            })
            .then(({firstSubject, firstTopic, subjects}) => {
                Client.getItems(firstSubject, firstTopic, (items) => {
                    this.setState({
                        subjects: subjects,
                        selectedSubject: firstSubject,
                        selectedTopic: firstTopic,
                        selectedItems: items.data
                    });
                })
            })
    }

    selectSubject = (subject) => {
        this.setState({selectedSubject: subject});
    };

    selectTopic = (topic) => {
        Client.getItems(this.state.selectedSubject, topic, (items) => {
            this.setState({
                selectedTopic: topic,
                selectedItems: items.data
            });
        });
    };

    updateLocalSubjectCount = (subjects, subject, topic, mode) => {
        let topicObj = subjects.find(s => s.subject === subject)     		  // need to decrement the topic
            .topics.find(t => t.name === topic);  // so we need to find it first
        if (mode === 'increment') {
            topicObj.count++;
        } else if (mode === 'decrement') {
            topicObj.count--;
        } else {
            throw new Error('Invalid mode in updateLocalSubjectCount. Needs to be "increment" or "decrement"');
        }
    };

    deleteItem = (e, index) => { // index is the position of item we want to delete
        let updatedItems = [...this.state.selectedItems];
        updatedItems.splice(index, 1);
        let item = this.state.selectedItems[index];

        if (item.newItem) {
            // no need to update the store because
            // the new item has not been persisted there yet
            this.setState({selectedItems: updatedItems});
        } else {
            Client.deleteItem(item)
                .then(() => {
                    let updatedSubjects = [...this.state.subjects]; // unlike deleting a new, un-persisted item, deleting a persisted item is needed because the KnowledgeSubjectTree count must be updated in the store as well as in the subjects collection
                    this.updateLocalSubjectCount(updatedSubjects, item.subject, item.topic, 'decrement');
                    this.setState({
                        selectedItems: updatedItems,
                        subjects: updatedSubjects
                    });
                })
        }
    };

    updateItem = (id, newTitle, newDetails, newTests) => {
        let updatedItems = this.state.selectedItems.map((e) => e._id === id ? {
            ...e,
            title: newTitle,
            details: newDetails,
            tests: newTests
        } : e);
        let itemToUpdate = updatedItems.filter((x) => x._id === id)[0];
        let updatedSubjects = [...this.state.subjects];

        if (itemToUpdate.newItem === true) {
            Client.addItem(itemToUpdate, function(){
				for (let testToUpdate of itemToUpdate.tests) {
					Client.updateTest(testToUpdate);
				}
			}).then(() => {
                    itemToUpdate.newItem = false;
                    this.updateLocalSubjectCount(updatedSubjects, itemToUpdate.subject, itemToUpdate.topic, 'increment');
                    this.setState({
                        selectedItems: updatedItems,
                        subjects: updatedSubjects
                    });
                });
        } else {
            Client.updateItem(itemToUpdate, function () {
                for (let testToUpdate of itemToUpdate.tests) {
                    Client.updateTest(testToUpdate);
                }
            }).then(() => {
                this.setState({selectedItems: updatedItems});
            });
        }
    };

    reorderItem = ({oldIndex, newIndex}) => {
        let reorderedItems = arrayMove(this.state.selectedItems, oldIndex, newIndex);
        reorderedItems.map((item, index) => item.position = index);
        this.setState({selectedItems: reorderedItems});  // let's update the state independently of the db update
        Client.updateAllPositions(reorderedItems);
    };

    addNewItem = () => {
        let newItem = {
            _id: mongoose.Types.ObjectId(),
            position: this.state.selectedItems.length,
            subject: this.state.selectedSubject,
            topic: this.state.selectedTopic,
            title: "New Item",              // initial title helps user identify the new item on the screen
            tests: [],                      // no tests are available initially
            score: 0,                       // always start with a score of zero
            newItem: true
        };                  // temporary field used to drive accept/cancel behavior of new item
        let updatedItems = [newItem, ...this.state.selectedItems];

        // We only set selectedItems for a new item.
        this.setState({selectedItems: updatedItems});
        // It is in a "temporary" state until the user clicks on the checkmark button.
        // If the user clicks on the x button on this new item, the temporary item is
        // removed and the temporary item is never persisted onto storage.
    };

    render() {
        return (
            <div className={styles.browser}>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={5}>
                            <KnowledgeSubjectTree subjects={this.state.subjects}
                                                  activeTopic={this.state.selectedTopic}
                                                  onSelectSubject={this.selectSubject}
                                                  onSelectTopic={this.selectTopic}/>
                        </Grid.Column>
                        <Grid.Column width={11}>
                            <Menu secondary>
                                <Menu.Item>
                                    <Button primary icon labelPosition='right' onClick={this.addNewItem}>
                                        Add Item
                                        <Icon name='plus'/>
                                    </Button>
                                </Menu.Item>
                            </Menu>
                            <KnowledgeItemList selectedItems={this.state.selectedItems}
                                               onDeleteItem={this.deleteItem}
                                               onReorderItem={this.reorderItem}
                                               onUpdateItem={this.updateItem}
                                               onNewTest={this.addNewTest}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

export default KnowledgeSubjectBrowser;
