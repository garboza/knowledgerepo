import React from 'react';
import KnowledgeItemRow from './item/KnowledgeItemRow';
import './item/KnowledgeItem.css';
import { ReactComponent as KnowledgeItemReorderIcon } from './item/KnowledgeItemReorderIcon.svg';
import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc';

const DragHandle = sortableHandle(() => <KnowledgeItemReorderIcon className="hamburgericon" />);

const SortableItem = sortableElement(({value, index, onDelete, onUpdate}) => 
	<KnowledgeItemRow id={value._id}
					  title={value.title} 
					  details={value.details}
					  tests={value.tests}
					  score={value.score}
					  newItem={value.newItem}
	                  index={index} 
	                  onDelete={onDelete} 
	                  onUpdate={onUpdate}
	                  handle={DragHandle}/>);

const SortableContainer = sortableContainer(({children}) => {
	return <div>{children}</div>
})

class KnowledgeItemList extends React.Component {

	render(){

		return (
			<SortableContainer onSortEnd={this.props.onReorderItem} helperClass={"SortableHelper"} useDragHandle>
				{this.props.selectedItems.map((value, index) => (
					<SortableItem key={`item-${value._id}`} 
								  index={index} 
								  value={value} 
								  onDelete={(e) => this.props.onDeleteItem(e, index)}
								  onUpdate={this.props.onUpdateItem} /> ))
			    }
			</SortableContainer>
		)
	}
}

export default KnowledgeItemList;