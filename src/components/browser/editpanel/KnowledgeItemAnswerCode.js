import React from 'react';
import QuillCodeBox from '../../quill/QuillCodeBox';
import { Grid } from 'semantic-ui-react';

class KnowledgeItemAnswerCode extends React.Component {

	handleUpdateSampleCode = (sampleCode) => {
		this.props.onChange( { sample_code : sampleCode } );
	}

	handleUpdateAnswer = (newAnswer) => {
		this.props.onChange( { answer : newAnswer } );
	}

	render() {

		return (
			<Grid>

				<Grid.Row columns={2}>
					<Grid.Column stretched>
						Sample Code :
						<QuillCodeBox readOnly={(this.props.mode === 'edit') ? false : true}
                                      value={this.props.sample}                                      
                                      onChange={this.handleUpdateSampleCode} />
					</Grid.Column>
					<Grid.Column stretched>	
						Answer :
						<QuillCodeBox readOnly={(this.props.mode === 'edit') ? false : true}
                              		  value={this.props.answer}
                              		  onChange={this.handleUpdateAnswer} />					
					</Grid.Column>
				</Grid.Row>

			</Grid>
		)
	}
}

export default KnowledgeItemAnswerCode;