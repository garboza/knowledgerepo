import React from 'react';
import KnowledgeItemTest from './KnowledgeItemTest';
import QuillEditBox from '../../quill/QuillEditBox';
import styles from './KnowledgeItemEditPanel.css';
import { Menu, Icon } from 'semantic-ui-react';
import mongoose from 'mongoose';


class KnowledgeItemEditPanel extends React.Component {

    handleNewDetails = (value) => { this.props.onNewDetails(value) }
    
    handleAddTest = () => {
        // TODO : this is where there would be some logic for choosing the type of test
        let newTest = { _id           : mongoose.Types.ObjectId(),
                        test_type     : "code" , 
                        test_question : "New Test", 
                        sample_code   : "", 
                        answer        : "" };
        this.props.onNewTest(newTest);
    }

    handleTestChange = (newTestDetails) => {
        this.props.onNewTestDetails(this.props.currentTabIndex - 1, newTestDetails);
    }

    getTabContent = () => {

        if (this.props.currentTab === 'Description'){
            return <QuillEditBox readOnly={(this.props.mode === 'edit') ? false : true}
                                 value={this.props.details}
                                 onChange={this.handleNewDetails} />
 
        } else {
            let testIndex = this.props.currentTabIndex - 1;
            return <KnowledgeItemTest mode={this.props.mode}
                                      test={this.props.tests[testIndex]}
                                      handleTestChange={this.handleTestChange}/>
        }
    }

	render(){

      // TODO : this is getting re-rendered a lot
      // console.log("I am getting rerendered");   

      let availableTabs = ["Description", ...[...this.props.tests.keys()].map((e) => "Test " + (e + 1))];
		
      return (
			<div className={styles.panel}>
           <div className={styles.editor_container}>
             { this.getTabContent() }
           </div>
           <div className={styles.panel_menu}>
               <Menu icon vertical>
                   { availableTabs.map((e,index) => <Menu.Item name={e} 
                                                               key={index}
                                                               active={e === this.props.currentTab} 
                                                               onClick={this.props.onSelectNewTab} 
                                                               className={styles.panel_menu_item} >
                                                        <Icon name={ e === 'Description' ? 'file alternate outline' : 'flag checkered'} />
                                                    </Menu.Item> ) }
                   { this.props.mode === 'edit' && 
                     
                        <Menu.Item
                          name='Add Test'
                          onClick={this.handleAddTest}
                          className={styles.panel_menu_item}>
                            <Icon name='plus square outline' />
                        </Menu.Item>
                   }
               </Menu>
           </div>
			</div>

		)
	}
}

export default KnowledgeItemEditPanel;