import React from 'react';
import KnowledgeItemAnswerCode from './KnowledgeItemAnswerCode';
import QuillEditBox from '../../quill/QuillEditBox';

class KnowledgeItemTest extends React.Component {

	handleQuestionChange = (newQuestion) => {		
		this.props.handleTestChange({ test_question : newQuestion }); 
	}

    getAnswerComponent = () => {

    	// TODO : in the future I will have more types of tests (multiple choice, etc.)
    	if (this.props.test.test_type === 'code'){ 
    		return <KnowledgeItemAnswerCode mode={this.props.mode}
    										sample={this.props.test.sample_code}
    										answer={this.props.test.answer}    										
    										onChange={this.props.handleTestChange} />
    	}
    }

	render(){
		return (
			<div>
				Question : 
				<QuillEditBox readOnly={(this.props.mode === 'edit') ? false : true}
                              value={this.props.test.test_question}
                              bordered={true}                         
                              onChange={this.handleQuestionChange} />
                <br />
				{ this.getAnswerComponent() }
				
			</div>
		)
	}
}

export default KnowledgeItemTest;