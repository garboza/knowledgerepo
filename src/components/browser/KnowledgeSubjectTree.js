import React from 'react'
import {Accordion, Dimmer, Label, Loader, Menu} from 'semantic-ui-react'
import '../KnowledgeRepo.css'

export default class KnowledgeSubjectTree extends React.Component {

    getMenu = (topics) => {

        return (
            <Menu fluid vertical>
                {topics.map((t, index) => <Menu.Item name={t.name}
                                                     key={index}
                                                     className="krfont"
                                                     active={this.props.activeTopic === t.name}
                                                     onClick={() => {
                                                         this.props.onSelectTopic(t.name)
                                                     }}>
                        <Label color='blue'>{t.count}</Label>
                        {t.name}
                    </Menu.Item>
                )
                }
            </Menu>
        )
    };

    handleClick = (event, data) => {
        this.props.onSelectSubject(data.content);
    };

    render() {

        // This will always happen the first time the page is loaded since the call to fetch the data is asynch
        if (this.props.subjects.length === 0) return <div className="tempBox">
            <Dimmer active inverted>
                <Loader size='large'>Loading</Loader>
            </Dimmer>
        </div>;

        const rootPanels = this.props.subjects.map((subject, index) => {
            return {
                key: `panel-${index}`,
                title: subject.subject,
                content: {content: this.getMenu(subject.topics)}
            }
        });

        return (
            <Accordion defaultActiveIndex={0}
                       panels={rootPanels}
                       onTitleClick={(event, data) => {
                           this.handleClick(event, data)
                       }}
                       styled/>
        )
    }
}

