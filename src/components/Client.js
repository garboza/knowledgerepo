/* eslint-disable no-undef */
function getSubjects(cb) {
    return fetch(`/.netlify/functions/subjectRead`, {
        accept: 'application/json',
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .catch((error) => console.log(error.message));
}

function incrementSubjectCount(subject, topic, cb) {
    return fetch(`/.netlify/functions/subjectUpdateCount?subject=${subject}&topic=${topic}&mode=increment`, {
        accepts: 'application/json'
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .then((error) => console.log(error.message));
}

function decrementSubjectCount(subject, topic, cb) {
    return fetch(`/.netlify/functions/subjectUpdateCount?subject=${subject}&topic=${topic}&mode=decrement`, {
        accepts: 'application/json',
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .then((error) => console.log(error.message));
}

function getItems(subject, topic, cb) {
    return fetch(`/.netlify/functions/itemRead?subject=${subject}&topic=${topic}`, {
        accepts: 'application/json',
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .catch((error) => console.log(error.message));
}

function addItem(newItem, cb) {
    return fetch(`/.netlify/functions/itemCreate`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(newItem)
    }).then(checkStatus)
        .then(parseJSON)
        .then(incrementSubjectCount(newItem.subject, newItem.topic))
        .then(cb)
        .catch((error) => console.log(error.message));
}

function updateItem(updatedItem, cb) {
    return fetch(`/.netlify/functions/itemUpdate`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedItem)
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .catch((error) => console.log(error.message));
}

function updateTest(updatedTest, cb) {
    return fetch(`/.netlify/functions/testUpdate`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedTest)
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .catch((error) => console.log(error.message));
}

function updateAllPositions(updatedItems, cb) {
    let positionsArray = updatedItems.map((x) => {
        return {_id: x._id, position: x.position}
    })
    return fetch(`/.netlify/functions/itemBulkUpdate`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(positionsArray)
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .catch((error) => console.log(error.message));
}

function deleteItem(updatedItem, cb) {
    return fetch(`/.netlify/functions/itemDelete`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedItem)
    }).then(checkStatus)
        .then(parseJSON)
        .then(decrementSubjectCount(updatedItem.subject, updatedItem.topic))
        .then(cb)
        .catch((error) => console.log(error.message));
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        console.log(error); // eslint-disable-line no-console
        throw error;
    }
}

function prepareQuizItems(subject, cb) {
    return fetch(`/.netlify/functions/quizRead?subject=${subject}`, {
        accepts: 'application/json',
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .catch((error) => console.log(error.message));
}

function addTest(newTest, cb) {
    return fetch(`/.netlify/functions/testCreate`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(newTest)
    }).then(checkStatus)
        .then(parseJSON)
        .then(cb)
        .catch((error) => console.log(error.message));
}

function parseJSON(response) {
    return response.json();
}

const Client = {
    getSubjects,
    getItems,
    addItem,
    updateItem,
    updateTest,
    deleteItem,
    updateAllPositions,
    incrementSubjectCount,
    decrementSubjectCount,
    prepareQuizItems,
    addTest
};
export default Client;
