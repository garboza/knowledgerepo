import React from 'react';
import Client from '../Client';
import './Quizlet.css';
import {Button, Icon, Segment, Header, Grid, Progress} from "semantic-ui-react";

class Quizlet extends React.Component {

    state = {
        quizItems: [],
        currentItem: -1
    };

    componentDidMount() {
        Client.prepareQuizItems('JavaScript', (items) => {
            this.setState({
                quizItems: items.data,
                currentItem: 0
            });
        })
    }

    submitAnswer = () => {
        this.setState({currentItem: this.state.currentItem + 1});
    }

    render() {
        let content;

        if (this.state.currentItem < 0) {
            content = <Segment loading style={{height: '85%'}}></Segment>
        } else if (this.state.currentItem >= this.state.quizItems.length) {
            content =
                <Segment placeholder inverted color='green' style={{height: '85%'}}>
                    <Header icon textAlign='center'>
                        <Icon name='rocket'/>
                        Congrats! Quiz complete!
                    </Header>
                </Segment>
        } else {
            console.log(this.state.currentItem);
            console.log(this.state.quizItems[this.state.currentItem].test_question);
            content =
                <Segment style={{height: '85%'}}>
                    <Grid>
                        <Grid.Row columns={2} divided style={{height: '360px'}}>
                            <Grid.Column>
                                Question
                            </Grid.Column>
                            <Grid.Column>
                                Sample code
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={1} >
                            <Grid.Column>
                                <Progress size='tiny' color='green' value={this.state.currentItem} total={this.state.quizItems.length} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Segment>
        }

        return (

            <Segment.Group style={{width: "800px", height: "50vh"}} stacked>
                {content}
                <Segment clearing>
                    <div  floated='right' className='ui two buttons'>
                        <Button basic color='red'>
                            Skip
                        </Button>
                        <Button basic color='green' onClick={this.submitAnswer}>
                            Submit
                        </Button>
                    </div>
                </Segment>
            </Segment.Group>

        )
    }
}

export default Quizlet;
