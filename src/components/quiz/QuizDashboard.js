import React from 'react';
import Quizlet from './Quizlet';
import { Button } from 'semantic-ui-react';
import './QuizDashboard.css';

class QuizDashboard extends React.Component {

	state = { quizState : 'dashboard' };

	handleStartQuiz = () => {
		this.setState({ quizState : 'quizStarted' });
	};

	getQuizContent = () => {
		switch (this.state.quizState) {
			case 'dashboard':
				return <Button primary onClick={this.handleStartQuiz}>Start Quiz</Button>;
			case 'quizStarted':
				return <Quizlet />;
			default:
				return <Quizlet />;
		}
	}

	render(){
		return (
			<div className="dashboard">
				{this.getQuizContent()}
			</div>
		)
	}
}

export default QuizDashboard;