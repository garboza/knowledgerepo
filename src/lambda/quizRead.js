import mongoose from 'mongoose';
import db from './server';
import Test from './testModel';

exports.handler = async (event, context) => {
	context.callbackWaitsForEmptyEventLoop = false;

	let subject = event['queryStringParameters']['subject'];

	try {
		const tests = await Test.find({}, function(err){
			if (err){
				console.log("Error reading tests");
			}
		})

		const response = {
			  	msg: "Quiz items found",
			  	data: tests
		}

		return {
			statusCode: 200,
			body: JSON.stringify(response)
		}
	} catch (err) {
		console.log(err);
		return {
			statusCode: 500,
			body: JSON.stringify({msg: err.message})
		}
	}

}
