import db from './server'
import Item from './itemModel'
exports.handler = async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false

    try {
        const data = JSON.parse(event.body),
            id = data._id,
            response = {
                msg: "Item successfully updated",
                data: data
            }

        await Item.findOneAndUpdate({_id: id}, data)
        return {
            statusCode: 201,
            body: JSON.stringify(response)
        }
    } catch (err) {
        console.log('item.update', err) // output to netlify function log
        return {
            statusCode: 500,
            body: JSON.stringify({msg: err.message})
        }
    }
}
