import db from './server'
import Item from './itemModel'
import Test from './testModel'

exports.handler = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false

  try {
    const data = JSON.parse(event.body);
    const id = data._id;
    const response = { msg: "Item successfully deleted" }

    // TODO : chain the deletion of the tests and the deletion of the item itself. If any test deletion fails then the whole thing should fail

    for (let test of data.tests){
        Test.findOneAndDelete({_id: test._id}, function(error){
          if (error) { console.log(error) }
        })
    }

    await Item.findOneAndDelete({_id: id});
    return {
      statusCode: 201,
      body: JSON.stringify(response)
    }
  } catch (err) {
    console.log('item.delete', err);
    return {
      statusCode: 500,
      body: JSON.stringify({msg: err.message})
    }
  }
}
