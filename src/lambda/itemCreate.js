import mongoose from 'mongoose'
import db from './server'
import Item from './itemModel'
import Test from './testModel'

exports.handler = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false

  try {
    const data = JSON.parse(event.body),
          response = {
            msg: "Item successfully created",
            data: data
          }

    await Item.create(data, function(err, createdItem){
        for (let newTest of data.tests){
            newTest.item = createdItem._id;
            console.log(newTest);
            Test.create(newTest);
        }
    })

return {
      statusCode: 201,
      body: JSON.stringify(response)
    }
  } catch (err) {
    console.log('item.create', err) // output to netlify function log
    return {
      statusCode: 500,
      body: JSON.stringify({msg: err.message})
    }
  }
}
