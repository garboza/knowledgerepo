import mongoose from 'mongoose';
import Item from './itemModel';

const testSchema = new mongoose.Schema({
	test_type     : String,
	test_question : String,
	sample_code   : String,
	answer        : String,
	item          : { type: mongoose.Schema.Types.ObjectId,
					   ref: 'Item' }
});


const Test = mongoose.model('test', testSchema);

export default Test;
