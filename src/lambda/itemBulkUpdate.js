import db from './server'
import Item from './itemModel'
exports.handler = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false
  
  try {
    const data = JSON.parse(event.body),
          response = {
            msg: "Items successfully updated",
            data: data
          }

    data.map((x) => {
      Item.updateOne({ _id : x._id }, { $set : { "position" : x.position }})    // NOTE : no await used here. should I?
          .then( returned => {
            // console.log(JSON.stringify(returned));
          });
    });

    // TODO for some reason this doesn't work :(
    // const bulkThing = data.map((x) => { return {
    //   updateOne :
    //         {
    //            "filter" : { "_id" : x._id },
    //            "update" : { $set : { "position" : x.position } }
    //         }
    // }});
    // await db.collections.items.bulkWrite(bulkThing).then( bulkWriteOpResult => {
    //   console.log('BULK update OK');
    //   console.log(JSON.stringify(bulkWriteOpResult, null, 2));
    // })

    return {
      statusCode: 201,
      body: JSON.stringify(response)
    }
  } catch (err) {
    console.log('item.update', err) // output to netlify function log
    return {
      statusCode: 500,
      body: JSON.stringify({msg: err.message})
    }
  }
}