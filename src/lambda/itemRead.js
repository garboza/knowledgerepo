import mongoose from 'mongoose';
import db from './server';
import Item from './itemModel';

exports.handler = async (event, context) => {
	context.callbackWaitsForEmptyEventLoop = false;

	let subject = event['queryStringParameters']['subject'];
	let topic = event['queryStringParameters']['topic'];

	try {
		const items = await Item.where('subject', subject)
		 						.where('topic', topic)
		 						.populate('tests')
		 						.sort('position'),
			  response = {
			  	msg: "Items successfully found",
			  	data: items
			  }

		return {
			statusCode: 200,
			body: JSON.stringify(response)
		}
	} catch (err) {
		console.log(err);
		return {
			statusCode: 500,
			body: JSON.stringify({msg: err.message})
		}
	}

}
