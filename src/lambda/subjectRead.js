import mongoose from 'mongoose';
import db from './server';
import Subject from './subjectModel';

exports.handler = async (event, context) => {
	context.callbackWaitsForEmptyEventLoop = false;
	
	try {
		const subjects = await Subject.find(),
			  response = {
			  	msg: "Subjects successfully found",
			  	data: subjects
			  }

		return {
			statusCode: 200,
			body: JSON.stringify(response)
		}
	} catch (err) {
		console.log(err);
		return {
			statusCode: 500,
			body: JSON.stringify({msg: err.message})
		}
	}

}