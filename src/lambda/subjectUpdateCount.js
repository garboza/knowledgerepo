import db from './server'
import Subject from './subjectModel'
exports.handler = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;

  let subject = event['queryStringParameters']['subject'];
  let topic = event['queryStringParameters']['topic'];
  let mode = event['queryStringParameters']['mode'];
  let incrementAmount;
  
  try {
    const response = {
            msg: "Subject count successfully updated"
          };

    if (mode === 'increment') {
      incrementAmount = 1;
    } else if (mode === 'decrement') {
      incrementAmount = -1;
    } else {
      console.log(`ERROR, mode needs to be "increment" or "decrement", but you passed in : (${mode})`);
    }

    await Subject.findOne({subject: subject})
            .then((foundSubject) => {
              var foundTopic;
              for (var curr of foundSubject.topics) {                
                if (curr.get('name') === topic){
                  foundTopic = curr;
                  break;
                } 
              }

              curr.count = curr.count + incrementAmount;
              foundSubject.save();
            });

    return {
      statusCode: 201,
      body: JSON.stringify(response)
    }
  } catch (err) {
    console.log('subjectUpdateCount', err) // output to netlify function log
    return {
      statusCode: 500,
      body: JSON.stringify({msg: err.message})
    }
  }
}