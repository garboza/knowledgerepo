import mongoose from 'mongoose';

const topicSchema = new mongoose.Schema({
	topic: String,
	count: Number
});

const subjectSchema = new mongoose.Schema({
	subject: {
		type: String,
		required: [true, 'Subject name field is required'],
		max: 30
	},
	topics: {
		type: [topicSchema],
		required: [false]
	}
});

const Subject = mongoose.model('subject', subjectSchema);

export default Subject;