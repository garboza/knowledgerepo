import mongoose from 'mongoose';
import Test from './testModel'; // without this import we get an error : MissingSchemaError: Schema hasn't been registered for model "test"

const itemSchema = mongoose.Schema({
	position : Number,
	subject  : String,
	topic    : String,
	details  : String,
	score    : Number,
	title: {
		type: String,
		required: [true, 'Item title field is required'],
		max: 50
	},
	tests: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'test'
	}]
});

const Item = mongoose.model('item', itemSchema);

export default Item;